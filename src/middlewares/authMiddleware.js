import axios from 'axios'
import { AUTH_API } from 'config'
import { resError } from 'utils/res'
import Errors from 'constants/errors'

/**
 * Get Token
 * @param req
 * @returns {*|executor|CancelToken}
 */
function getAuthorizationToken (req) {
  return req.headers['authorization'] || req.query.token || req.body.token
}

/**
 * Connect to API fetch user data by token
 * @param token
 */
async function getAuthMe (token) {
  const res = await axios.get(AUTH_API + '/auth/me', {
    headers: {
      authorization: token
    }
  })
  if (res.data.error) {
    return false
  } else {
    return res.data.data
  }
}

/**
 * Protected route
 * @param req
 * @param res
 * @param next
 */
export default async function authMiddleware (req, res, next) {
  const authToken = getAuthorizationToken(req)
  if (!authToken) {
    resError(res, Errors.NOT_AUTHENTICATED)
  } else {
    const user = await getAuthMe(authToken)
    if (!user) {
      resError(res, Errors.NOT_AUTHENTICATED)
      return
    }
    // Assign user to req
    req.user = user
    next()
  }
}
