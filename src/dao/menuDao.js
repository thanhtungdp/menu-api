import Menu from 'models/Menu'

export default {
	/**
   * Check exists menu
	 * @param role
	 * @param name
	 * @returns {Query|*}
	 */
  async checkExists ({ role, name }) {
    const menuExists = await Menu.findOne({ role, name })
    return menuExists
  },

	/**
   * Create menu
	 * @param role
	 * @param name
	 * @param menuList
	 */
  async createMenu ({ role, name, menuList = [] }) {
    const menu = new Menu({
      role,
      name,
      menuList
    })
    await menu.save()
    return menu
  },

	/**
   * Get menu
	 * @param role
	 * @param name
	 * @returns {Query|*}
	 */
  async getMenu ({ role, name = 'default' }) {
    const menu = await Menu.findOne({ role, name })
    return menu
  }
}
