import mongoose from 'mongoose'

const Menu = new mongoose.Schema({
  role: String,
  name: String,
  menuList: [{ id: String, name: String, url: String }],
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now }
})

export default mongoose.model('Menu', Menu)
