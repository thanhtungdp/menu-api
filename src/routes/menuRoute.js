import express from 'express'
import authMiddleware from 'middlewares/authMiddleware'
import menuDao from 'dao/menuDao'
import { resError } from 'utils/res'
import Errors from 'constants/errors'

const router = express.Router()

/**
 * Get Menu by name
 */
router.get('/:name', authMiddleware, async (req, res) => {
  const menu = await menuDao.getMenu({
    role: req.user.role,
    name: req.params.name
  })
  if (!menu) {
    resError(res, Errors.MENU_NOT_EXISTS)
  } else {
    res.json({
      data: menu
    })
  }
})

export default router
